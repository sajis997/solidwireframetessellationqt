#ifndef TESSELLATIONSCENEITEM_H
#define TESSELLATIONSCENEITEM_H

#include <QTime>
#include <QtQuick/QQuickItem>

class QTimer;
class TeapotTessellation;

/*
 *
 *
 */
class TessellationSceneItem : public QQuickItem
{
    Q_OBJECT
    //the property defintion must be at the top of the class
    //the notify signal is needed for correct property bindings
    Q_PROPERTY(float outer READ outer WRITE setOuter NOTIFY outerChanged)
    Q_PROPERTY(float inner READ inner WRITE setInner NOTIFY innerChanged)
    Q_PROPERTY(float maxTessLevel READ maxTessLevel NOTIFY maxTessLevelChanged)

public:
    TessellationSceneItem();

    void setOuter(float);
    void setInner(float);

    float inner();
    float outer();

    float maxTessLevel();

signals:
    void innerChanged();
    void outerChanged();
    void maxTessLevelChanged();


public slots:
    void updateTime();
    void sync();
    void cleanup();

    void setMaxTexLevel(float);

private slots:
    void handleWindowChanged(QQuickWindow *);

private:

    TeapotTessellation* mTessScene;

    QTime mTIme;

    QTimer *mTimer;

    float mMaxTessLevel;

};


#endif // TESSELLATIONSCENEITEM_H
