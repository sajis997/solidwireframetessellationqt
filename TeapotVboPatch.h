#ifndef TEAPOTVBOPATCH_H
#define TEAPOTVBOPATCH_H

#include <QOpenGLFunctions_4_3_Compatibility>

#include "defines.h"
#include "teapotdata.h"

class GLSLShader;

class TeapotVboPatch : protected QOpenGLFunctions_4_3_Compatibility
{
private:
    GLuint mTeapotVAOID;
    GLuint mTeapotBufferID;

    void generatePatches(float *v);
    void buildPatchReflect(int patchNum,
                           float *v,
                           int &index,
                           bool reflectX,
                           bool reflectY);

    void buildPatch(glm::vec3 patch[][4],
                    float *v,
                    int &index,
                    glm::mat3 reflect);

    void getPatch(int patchNum,glm::vec3 patch[][4],bool reverseV);

    GLSLShader *mShader;

public:

    TeapotVboPatch(GLSLShader *);
    ~TeapotVboPatch();

    void render();

};

#endif // TEAPOTVBOPATCH_H
