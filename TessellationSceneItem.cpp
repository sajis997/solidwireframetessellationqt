#include <QtGui/QOpenGLContext>
#include <QtQuick/QQuickWindow>
#include <QTimer>

#include "TeapotTessellation.h"
#include "TessellationSceneItem.h"

TessellationSceneItem::TessellationSceneItem()
    :mTessScene(0),
     mMaxTessLevel(1.0)
{

    //initialize the tessellation renderer
    mTessScene = new TeapotTessellation();

    //start tracking the time
    mTIme.start();

    mTimer = new QTimer;
    mTimer->start();

    //the constructor simply initializes the values and connects to the window chaged signal
    //that we shall use to prepare our renderer
    connect(this,SIGNAL(windowChanged(QQuickWindow*)),this,SLOT(handleWindowChanged(QQuickWindow*)));

}

void TessellationSceneItem::handleWindowChanged(QQuickWindow *win)
{
    if(win)
    {
        //define the opengl surface format
        //with the most modern profile
        //while retaining the compatibility profile
        QSurfaceFormat f = win->format();
        f.setMajorVersion(4);
        f.setMinorVersion(3);
        f.setSamples(4);
        f.setStencilBufferSize(8);
        f.setProfile(QSurfaceFormat::CompatibilityProfile);

        win->setFormat(f);

        win->setClearBeforeRendering(false);

        //Qt::DirectConnection
        //The slot is invoked immediately when the signal is emitted.
        //The slot is executed in the signalling thread.
        connect(win,SIGNAL(beforeSynchronizing()),this,SLOT(sync()),Qt::DirectConnection);
        /*
         * when the sceneGraphInvalidated signal is emitted. This can happen, for instance
         * when the window is hidden. Then the slot cleanup will be called on the rendering
         * thread while the GUI thread is blocked.
         *
        */
        connect(win,SIGNAL(sceneGraphInvalidated()),this,SLOT(cleanup()),Qt::DirectConnection);

        //make the connection to render the raw opengl scene before the scene graph rendering
        //and we make sure that the slot finishes the rendering before further execution
        connect(win,SIGNAL(beforeRendering()),mTessScene,SLOT(paint()),Qt::DirectConnection);

        connect(mTessScene,SIGNAL(tessellationLevelChanged(float)),this,SLOT(setMaxTexLevel(float)),Qt::DirectConnection);
    }
}


void TessellationSceneItem::cleanup()
{
    /*
     * delete the scene and cleanup the allocated memory
     * */
    if(mTessScene)
    {
        delete mTessScene;
        mTessScene = 0;
    }
}

/*
 * the following slot is from the signal beforeSynchronizing() that is emitted
 * on the rendering thread while the GUI thread is blocked, so it is safe to simply
 * copy the value without any additional protection
 * */
void TessellationSceneItem::sync()
{
    //if the opengl scene is initialized we copy the state of the item
    //into the renderer
    if(mTessScene)
    {
        connect(mTimer,SIGNAL(timeout()),this,SLOT(updateTime()),Qt::DirectConnection);

        mTessScene->setViewportSize(window()->width() * window()->devicePixelRatio(),
                                    window()->height() * window()->devicePixelRatio());
    }
}

void TessellationSceneItem::updateTime()
{
    float time = mTIme.elapsed() * 0.001;

    mTessScene->update(time);

    //ask QQuickView to render the scene
    window()->update();
}

void TessellationSceneItem::setOuter(float outerFactor)
{
    if(outerFactor == mTessScene->outerTessellationFactor())
        return;

    mTessScene->setOuterTessellationFactor(outerFactor);
    //do not emit notifier if value does not actually change
    emit outerChanged();
}

void TessellationSceneItem::setInner(float innerFactor)
{
    if(innerFactor == mTessScene->innerTessellationFactor())
        return;

    mTessScene->setInnerTessellationFactor(innerFactor);
    emit innerChanged();
}

float TessellationSceneItem::inner()
{
    return (float)(mTessScene->innerTessellationFactor());
}

float TessellationSceneItem::outer()
{
    return (float)(mTessScene->outerTessellationFactor());
}

float TessellationSceneItem::maxTessLevel()
{
    return mMaxTessLevel;
}

void TessellationSceneItem::setMaxTexLevel(float level)
{
    if(level == maxTessLevel())
        return;

    mMaxTessLevel = level;

    emit maxTessLevelChanged();
}



