include ( common/common.pri )

TEMPLATE = app

INCLUDEPATH += common

HEADERS += \
    TessellationSceneItem.h \
    TeapotVboPatch.h \
    TeapotTessellation.h \
    teapotdata.h

SOURCES += \
    TeapotVboPatch.cpp \
    TeapotTessellation.cpp \
    TessellationSceneItem.cpp \
    main.cpp


RESOURCES += TeapotTessellationSceneItem.qrc

OTHER_FILES += shaders/solidwiredframe.vert \
               shaders/solidwiredframe.tcs \
               shaders/solidwiredframe.tes \
               shaders/solidwiredframe.geom \
               shaders/solidwiredframe.frag
