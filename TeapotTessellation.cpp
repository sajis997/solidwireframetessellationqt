#include <QTimer>

#include "TeapotTessellation.h"
#include "GLSLShader.h"
#include "TeapotVboPatch.h"

TeapotTessellation::TeapotTessellation(QObject *parent)
    : AbstractScene(parent),
      mTessellationShader(0),
      mTeapotPatch(0),
      mInnerTessellationFactor(1.0f),
      mOuterTessellationFactor(1.0f),
      mZoom(22.0f),
      mInitialized(false),
      mCurrentTime(0.0f)
{


}

TeapotTessellation::~TeapotTessellation()
{
    if(mTessellationShader)
    {
        mTessellationShader->DeleteShaderProgram();
        delete mTessellationShader;
        mTessellationShader = 0;
    }

    if(mTeapotPatch)
    {
        delete mTeapotPatch;
        mTeapotPatch = 0;
    }
}

void TeapotTessellation::setOuterTessellationFactor(float outerFactor)
{
    mOuterTessellationFactor = outerFactor;
}

void TeapotTessellation::setInnerTessellationFactor(float innerFactor)
{
    mInnerTessellationFactor = innerFactor;
}

GLfloat TeapotTessellation::innerTessellationFactor()
{
    return mInnerTessellationFactor;
}

GLfloat TeapotTessellation::outerTessellationFactor()
{
    return mOuterTessellationFactor;
}


void TeapotTessellation::initialise()
{
    //return if already initialized
    if(mInitialized)
        return;

    //initialize the opengl functions
    if(!initializeOpenGLFunctions())
    {
        std::cerr << "Modern OpenGL functions with the compatibility profile cannot be initialized." << std::endl;
        exit(EXIT_FAILURE);
    }

    //load and compile shaders
    loadShaders();

    //initialize the teapot patch
    mTeapotPatch = new TeapotVboPatch(mTessellationShader);

    GLint lMaxTessLevel;

    //get the maximium number of tessellation factors
    //supoorted by the current opengl driver
    glGetIntegerv(GL_MAX_TESS_GEN_LEVEL,&lMaxTessLevel);

    float lMaxTessellationLevel = 0.0;

    //update the member variable
    lMaxTessellationLevel = (float)lMaxTessLevel;

    //emit the signal that the tessellation level
    //has updated and this signal will be connected
    //with the slot in tessellationsceneitem
    emit tessellationLevelChanged(lMaxTessellationLevel);

    ModelViewMatrix = glm::mat4(1.0f);
    ProjectionMatrix = glm::mat4(1.0f);
    ViewportMatrix = glm::mat4(1.0f);

    ViewMatrix = glm::mat4(1.0f);
    ModelMatrix = glm::mat4(1.0f);

    NormalMatrix = glm::mat3(1.0f);

    mInitialized = true;
}


void TeapotTessellation::loadShaders()
{
    mTessellationShader = new GLSLShader();

    mTessellationShader->LoadFromFile(GL_VERTEX_SHADER,"shaders/solidwiredframe.vert");
    mTessellationShader->LoadFromFile(GL_TESS_CONTROL_SHADER,"shaders/solidwiredframe.tcs");
    mTessellationShader->LoadFromFile(GL_TESS_EVALUATION_SHADER,"shaders/solidwiredframe.tes");
    mTessellationShader->LoadFromFile(GL_GEOMETRY_SHADER,"shaders/solidwiredframe.geom");
    mTessellationShader->LoadFromFile(GL_FRAGMENT_SHADER,"shaders/solidwiredframe.frag");

    mTessellationShader->CreateAndLinkProgram();

    mTessellationShader->Use();

    mTessellationShader->AddUniform("inner");
    mTessellationShader->AddUniform("outer");
    mTessellationShader->AddUniform("ModelviewMatrix");
    mTessellationShader->AddUniform("ProjectionMatrix");
    mTessellationShader->AddUniform("NormalMatrix");
    mTessellationShader->AddUniform("ViewportMatrix");

    mTessellationShader->AddUniform("Line.Width");
    mTessellationShader->AddUniform("Line.Color");

    mTessellationShader->AddUniform("Light.Position");
    mTessellationShader->AddUniform("Light.Intensity");

    mTessellationShader->AddUniform("Material.Kd");
    mTessellationShader->AddUniform("Material.Ka");
    mTessellationShader->AddUniform("Material.ks");
    mTessellationShader->AddUniform("Material.Shininess");

    glUniform1f((*mTessellationShader)("Line.Width"),0.8f);

    //define the color of the line
    glm::vec4 LineColor = glm::vec4(0.05f,0.0f,0.05f,1.0f);

    //send the line color to the shader
    glUniform4fv((*mTessellationShader)("Line.Color"),1,glm::value_ptr(LineColor));

    //define the light position
    glm::vec4 LightPosition = glm::vec4(0.25f,0.25f,1.0f,1.0f);

    glUniform4fv((*mTessellationShader)("Light.Position"),1,glm::value_ptr(LightPosition));

    //define the light intensity
    glm::vec3 LightIntensity = glm::vec3(1.f,1.f,1.f);

    glUniform3fv((*mTessellationShader)("Light.Intensity"),1,glm::value_ptr(LightIntensity));

    //now set and send the material property
    glm::vec3 MaterialAmbientReflectivity = glm::vec3(0.2f,0.2f,0.2f);

    glUniform3fv((*mTessellationShader)("Material.Ka"),1,glm::value_ptr(MaterialAmbientReflectivity));

    glm::vec3 MaterialDiffuseReflectivity = glm::vec3(0.7f,0.7f,0.7f);

    glUniform3fv((*mTessellationShader)("Material.Kd"),1,glm::value_ptr(MaterialDiffuseReflectivity));

    glm::vec3 MaterialSpecularReflectivity = glm::vec3(0.8f,0.8f,0.8f);

    glUniform3fv((*mTessellationShader)("Material.Ks"),1,glm::value_ptr(MaterialSpecularReflectivity));

    float MaterialShininess = 100.0f;

    glUniform1f((*mTessellationShader)("Material.Shininess"),MaterialShininess);

    mTessellationShader->UnUse();
}

void TeapotTessellation::update(float t)
{
    mCurrentTime = t;
}

void TeapotTessellation::render()
{
    //set the clearing color for the background
    glClearColor(0.5f,0.5f,0.5f,1.0f);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glm::mat4 Ty = glm::translate(glm::mat4(1.0f),glm::vec3(0.0f,0.5f,0.0f));
    glm::mat4 Tz = glm::translate(glm::mat4(1.0f),glm::vec3(0.0f,0.0f,-mZoom));
    glm::mat4 Ry = glm::rotate(glm::mat4(1.0),mCurrentTime,glm::vec3(0.0f,1.0f,0.0f));
    glm::mat4 Rx = glm::rotate(glm::mat4(1.0f),mCurrentTime,glm::vec3(1.0f,0.0f,0.0f));
    glm::mat4 Rz = glm::rotate(glm::mat4(1.0f),mCurrentTime,glm::vec3(0.0f,0.0f,1.0f));

    ModelMatrix = Tz * Ty * Rx * Ry * Rz;

    ModelViewMatrix = ViewMatrix * ModelMatrix;

    NormalMatrix = glm::inverseTranspose(glm::mat3(ModelViewMatrix));

    //enable depth testing
    glEnable(GL_DEPTH_TEST);

    glDepthFunc(GL_LEQUAL);

    glEnable(GL_CULL_FACE);

    //THE FOLLOWING IS THE SAVER - FOR THE HIDDEN SURFACE REMOVAL ISSUE
    //REASONING
    //1.it seems that the qml stuff might leave the depth mask to false state
    glDepthMask(true);

    mTessellationShader->Use();
    ////////////////////////////
//    glUniform1f((*mTessellationShader)("Line.Width"),0.8f);

//    //define the color of the line
//    glm::vec4 LineColor = glm::vec4(0.05f,0.0f,0.05f,1.0f);

//    //send the line color to the shader
//    glUniform4fv((*mTessellationShader)("Line.Color"),1,glm::value_ptr(LineColor));

//    //define the light position
//    glm::vec4 LightPosition = glm::vec4(0.25f,0.25f,1.0f,1.0f);

//    glUniform4fv((*mTessellationShader)("Light.Position"),1,glm::value_ptr(LightPosition));

//    //define the light intensity
//    glm::vec3 LightIntensity = glm::vec3(1.f,1.f,1.f);

//    glUniform3fv((*mTessellationShader)("Light.Intensity"),1,glm::value_ptr(LightIntensity));

//    //now set and send the material property
//    glm::vec3 MaterialAmbientReflectivity = glm::vec3(0.2f,0.2f,0.2f);

//    glUniform3fv((*mTessellationShader)("Material.Ka"),1,glm::value_ptr(MaterialAmbientReflectivity));

//    glm::vec3 MaterialDiffuseReflectivity = glm::vec3(0.7f,0.7f,0.7f);

//    glUniform3fv((*mTessellationShader)("Material.Kd"),1,glm::value_ptr(MaterialDiffuseReflectivity));

//    glm::vec3 MaterialSpecularReflectivity = glm::vec3(0.8f,0.8f,0.8f);

//    glUniform3fv((*mTessellationShader)("Material.Ks"),1,glm::value_ptr(MaterialSpecularReflectivity));

//    float MaterialShininess = 100.0f;

//    glUniform1f((*mTessellationShader)("Material.Shininess"),MaterialShininess);

    ////////////////////////////
    glUniformMatrix4fv((*mTessellationShader)("ModelviewMatrix"),1,GL_FALSE,glm::value_ptr(ModelViewMatrix));
    glUniformMatrix4fv((*mTessellationShader)("ProjectionMatrix"),1,GL_FALSE,glm::value_ptr(ProjectionMatrix));
    glUniformMatrix4fv((*mTessellationShader)("ViewportMatrix"),1,GL_FALSE,glm::value_ptr(ViewportMatrix));
    glUniformMatrix3fv((*mTessellationShader)("NormalMatrix"),1,GL_FALSE,glm::value_ptr(NormalMatrix));
    glUniform1f((*mTessellationShader)("outer"),mOuterTessellationFactor);
    glUniform1f((*mTessellationShader)("inner"),mInnerTessellationFactor);

    mTeapotPatch->render();

    mTessellationShader->UnUse();

    glDepthMask(false);

}

void TeapotTessellation::resize(int w,int h)
{
    if(h < 1)
         h = 1;

    //get the updated viewport width and height
    mViewportWidth = w;
    mViewportHeight = h;

    //set the viewport matrix
    ViewportMatrix = glm::mat4(glm::vec4((GLfloat)mViewportWidth/2.0f,0.0f,0.0f,0.0f),
                               glm::vec4(0.0f,(GLfloat)mViewportHeight/2.0f,0.0f,0.0f),
                               glm::vec4(0.0f,0.0f,1.0f,0.0f),
                               glm::vec4((GLfloat)mViewportWidth/2.0f,(GLfloat)mViewportHeight/2.0f,0.0f,1.0f));

    GLfloat aspect = mViewportWidth/(GLfloat)mViewportHeight;

    //setup the projection matrix
    ProjectionMatrix = glm::perspective(50.0f,aspect,0.1f,1000.0f);

    glViewport(0,0,mViewportWidth,mViewportHeight);
}

void TeapotTessellation::setViewportSize(int width,int height)
{
    mViewportWidth = width;
    mViewportHeight = height;
}

void TeapotTessellation::paint()
{
    if(!mInitialized)
        initialise();

    resize(mViewportWidth,mViewportHeight);
    render();
}
