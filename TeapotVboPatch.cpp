#include "TeapotVboPatch.h"
#include "GLSLShader.h"

//each patch is made of 16 vertices
void TeapotVboPatch::generatePatches(float *v)
{
    int idx = 0;

    //build each patch

    //the rim
    buildPatchReflect(0,v,idx,true,true);

    //the body
    buildPatchReflect(1,v,idx,true,true);
    buildPatchReflect(2,v,idx,true,true);

    //the lid
    buildPatchReflect(3,v,idx,true,true);
    buildPatchReflect(4,v,idx,true,true);

    //the bottom
    buildPatchReflect(5,v,idx,true,true);

    //the handle
    buildPatchReflect(6,v,idx,false,true);
    buildPatchReflect(7,v,idx,false,true);

    //the spout
    buildPatchReflect(8,v,idx,false,true);
    buildPatchReflect(9,v,idx,false,true);


}

void TeapotVboPatch::buildPatchReflect(int patchNum,
                                       float *v,
                                       int &index,
                                       bool reflectX,
                                       bool reflectY)
{
    glm::vec3 patch[4][4];
    glm::vec3 patchRevV[4][4];

    getPatch(patchNum,patch,false);
    getPatch(patchNum,patchRevV,true);

    //patch without modification
    buildPatch(patchRevV,v,index,glm::mat3(1.0f));

    //patch reflected in X
    if(reflectX)
    {
        buildPatch(patch,
                   v,
                   index,
                   glm::mat3(glm::vec3(-1.0f,0.0f,0.0f),
                             glm::vec3(0.0f,1.0f,0.0f),
                             glm::vec3(0.0f,0.0f,1.0f)));
    }

    //patch reflected in Y
    if(reflectY)
    {
        buildPatch(patch,
                   v,
                   index,
                   glm::mat3(glm::vec3(1.0f,0.0f,0.0f),
                             glm::vec3(0.0f,-1.0f,0.0f),
                             glm::vec3(0.0f,0.0f,1.0f)));
    }

    //patch reflected in X and in Y
    if(reflectX && reflectY)
    {
        buildPatch(patchRevV,
                   v,
                   index,
                   glm::mat3(glm::vec3(-1.0f,0.0f,0.0f),
                             glm::vec3(0.0f,-1.0f,0.0f),
                             glm::vec3(0.0f,0.0f,1.0f)));
    }
}

void TeapotVboPatch::buildPatch(glm::vec3 patch[][4],
                float *v,
                int &index,
                glm::mat3 reflect)
{
    for(int i = 0; i < 4; i++)
    {
        for(int j = 0; j < 4; j++)
        {
            glm::vec3 pt = reflect * patch[i][j];

            v[index] = pt.x;
            v[index + 1] = pt.y;
            v[index + 2] = pt.z;

            index += 3;
        }
    }
}

void TeapotVboPatch::getPatch(int patchNum,glm::vec3 patch[][4],bool reverseV)
{
    for( int u = 0; u < 4; u++)
    {          // Loop in u direction
        for( int v = 0; v < 4; v++ )
        {     // Loop in v direction
            if( reverseV )
            {
                patch[u][v] = glm::vec3(
                                        Teapot::cpdata[Teapot::patchdata[patchNum][u*4+(3-v)]][0],
                                        Teapot::cpdata[Teapot::patchdata[patchNum][u*4+(3-v)]][1],
                                        Teapot::cpdata[Teapot::patchdata[patchNum][u*4+(3-v)]][2]
                                        );
            }
            else
            {
                patch[u][v] = glm::vec3(
                                        Teapot::cpdata[Teapot::patchdata[patchNum][u*4+v]][0],
                                        Teapot::cpdata[Teapot::patchdata[patchNum][u*4+v]][1],
                                        Teapot::cpdata[Teapot::patchdata[patchNum][u*4+v]][2]
                                        );
            }
        }
    }
}

TeapotVboPatch::TeapotVboPatch(GLSLShader *shader)
    : mShader(shader)
{
    //define the number of vertices
    //32 on each row and
    //16 on each column
    int verts = 32 * 16;

    //allocate memory for the vertices
    float *v = new float[verts * 3];

    if(!initializeOpenGLFunctions())
    {
        std::cerr << "Modern OpenGL Functions with the Compatibility Profile cannot be initialzied." << std::endl;
        std::exit(EXIT_FAILURE);
    }
    //generate the ID for the vertex array
    glGenVertexArrays(1,&mTeapotVAOID);
    glBindVertexArray(mTeapotVAOID);

    glGenBuffers(1,&mTeapotBufferID);

    //send the pointer to the buffer
    //generate patches with the vertices
    generatePatches(v);

    glBindBuffer(GL_ARRAY_BUFFER,mTeapotBufferID);
    glBufferData(GL_ARRAY_BUFFER,(3 * verts) * sizeof(float),v,GL_STATIC_DRAW);

    mShader->Use();

    //name an attribute with the location
    mShader->AddAttribute("Position");

    glVertexAttribPointer((*mShader)["Position"],3,GL_FLOAT,GL_FALSE,0,BUFFER_OFFSET(0));

    mShader->UnUse();

    delete [] v;

    glBindVertexArray(0);
}

TeapotVboPatch::~TeapotVboPatch()
{
    glDeleteBuffers(1,&mTeapotBufferID);
    glDeleteVertexArrays(1,&mTeapotVAOID);
}

void TeapotVboPatch::render()
{
    //i got some interesting artifacts if i do not set the
    //following function with the parameter

    /*
     * the following code tells the pipeline that
     * the size of the input patch is going to be 16
     * */
    glPatchParameteri(GL_PATCH_VERTICES,16);

    glBindVertexArray(mTeapotVAOID);
    glEnableVertexAttribArray((*mShader)["Position"]);

    glDrawArrays(GL_PATCHES,0,512);

    glDisableVertexAttribArray((*mShader)["Position"]);
    glBindVertexArray(0);
}

