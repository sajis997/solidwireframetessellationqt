import QtQuick 2.2
import QtQuick.Controls 1.1
import TeapotTessellation 1.0


Item {
    id: root
    width: 512; height: 512

    Text {
        text: "F1 - Show/Hide Settings"
        font.pixelSize: 15
        anchors.top: parent.top
        anchors.topMargin: 15
        anchors.right: parent.right
        anchors.rightMargin: 15
        opacity: 0.5
    }

    TessellationSceneItem
    {
        id: tessSceneItem
    }

    Grid {
        id: controlContainer
        rows: 2
        columns: 2
        spacing: 8
        anchors.margins: 8
        focus: true

        //initially hide the UI gridope
        x: -width

        property bool shown: false


        Text {
             id: outerTessellationText
             text: qsTr("Outer Tessellation Level: ")
             color: "black"
             opacity: 1.0
         }

         Slider {
             id: outerTessellationSlider
             opacity: 1.0
             minimumValue: 1.0
             maximumValue: tessSceneItem.maxTessLevel
             stepSize: 1.0

             onValueChanged: tessSceneItem.outer = value
         }


         Text {
             id: innerTessellationText
             text: qsTr("Inner Tessellation Level: ")
             color: "black"
             opacity: 1.0
         }



         Slider {
             id: innerTessellationSlider
             opacity: 1.0

             minimumValue: 1.0
             maximumValue: tessSceneItem.maxTessLevel
             stepSize: 1.0

             onValueChanged: tessSceneItem.inner = value
         }

         Behavior on x { SpringAnimation { spring: 1; damping: 0.1 } }
         Behavior on y { SpringAnimation { spring: 1; damping: 0.1 } }

    }

    Keys.onPressed: {
        if(event.key === Qt.Key_F1)
        {
            if(controlContainer.shown === true)
            {
              controlContainer.x = -controlContainer.width - 20
              controlContainer.shown = false
            }
            else
            {
              controlContainer.x = 0
              controlContainer.shown = true
            }
            event.accepted = true
        }
        else if(event.key === Qt.Key_Escape)
        {
            event.accepted = true
            Qt.quit()
        }
    }


    Rectangle {
        id: rect
        color: "red"
        radius: 10
        opacity: 0.1
        border.color: "black"
        focus: true
        //the rectangle element
        //will fill the whole layout
        anchors.fill: controlContainer
    }


}
