#version 430 compatibility

layout( triangles ) in;
layout( triangle_strip, max_vertices = 3) out;


in vec3 teNormal[];
in vec4 tePosition[];

out GS_OUT
{
  vec3 gNormal;
  vec4 gPosition; //it is camera coordinates
  noperspective vec3 gEdgeDistance;
} gs_out;

uniform mat4 ViewportMatrix;  // Viewport matrix

void main()
{
    // Transform each vertex into viewport space
    // we get the position in the clip coordinates
    vec3 p0 = vec3(ViewportMatrix * (gl_in[0].gl_Position / gl_in[0].gl_Position.w));
    vec3 p1 = vec3(ViewportMatrix * (gl_in[1].gl_Position / gl_in[1].gl_Position.w));
    vec3 p2 = vec3(ViewportMatrix * (gl_in[2].gl_Position / gl_in[2].gl_Position.w));

    vec2 v0 = vec2(p2 - p1);
    vec2 v1 = vec2(p2 - p0);
    vec2 v2 = vec2(p1 - p0);

    float area = abs(v1.x * v2.y - v1.y * v2.x);
    

    gs_out.gEdgeDistance = vec3( area/length(v0), 0, 0 );
    gs_out.gNormal = teNormal[0];
    gs_out.gPosition = tePosition[0];
    gl_Position = gl_in[0].gl_Position;
    EmitVertex();

    gs_out.gEdgeDistance = vec3( 0, area/length(v1), 0 );
    gs_out.gNormal = teNormal[1];
    gs_out.gPosition = tePosition[1];
    gl_Position = gl_in[1].gl_Position;
    EmitVertex();

    gs_out.gEdgeDistance = vec3( 0, 0, area/length(v2) );
    gs_out.gNormal = teNormal[2];
    gs_out.gPosition = tePosition[2];
    gl_Position = gl_in[2].gl_Position;
    EmitVertex();

    EndPrimitive();
}
