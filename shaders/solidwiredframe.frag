#version 430 compatibility

//encapsulate light information
struct LineInfo
{
    float Width;
    vec4 Color;
};

uniform LineInfo Line;

//encapsulate material info
struct MaterialInfo
{
   vec3 Ka; // ambient reflectivity
   vec3 Kd; // diffuse reflectivity
   vec3 Ks; // specular reflectivity
   float Shininess; // specular shininess factor
};

uniform MaterialInfo Material;

struct LightInfo
{
  vec4 Position;
  vec3 Intensity;
};

uniform LightInfo Light;

in GS_OUT
{
  vec3 gNormal;
  vec4 gPosition;
  noperspective vec3 gEdgeDistance;
} fs_in;


out vec4 FragColor;

vec3 phongModel(vec3 pos, vec3 norm)
{
    //vector to the light source	
    vec3 s = normalize(vec3(Light.Position) - pos);

    //view vector
    vec3 v = normalize(-pos.xyz);

    //reflection vector
    vec3 r = reflect( -s, norm );

    vec3 ambient = Light.Intensity * Material.Ka;
    float sDotN = max( dot(s,norm), 0.0 );
    vec3 diffuse = Light.Intensity * Material.Kd * sDotN;
    vec3 spec = vec3(0.0);

    if( sDotN > 0.0 )
        spec = Light.Intensity * Material.Ks *
               pow( max( dot(r,v), 0.0 ), Material.Shininess );

    return ambient + diffuse + spec;
}



float edgeMix()
{
    float d = min( min( fs_in.gEdgeDistance.x, fs_in.gEdgeDistance.y ), fs_in.gEdgeDistance.z );

    if( d < (Line.Width - 1 )) 
    {
        return 1.0;
    } 
    else if( d > (Line.Width + 1 )) 
    {
        return 0.0;
    } 
    else 
    {
        float x = d - (Line.Width - 1);
        return exp2(-2.0 * (x*x));
    }
}


void main()
{    	
    float mixVal = edgeMix();

    vec4 color = vec4( phongModel( fs_in.gPosition.xyz, fs_in.gNormal ), 1.0);
        
    FragColor = mix( color, Line.Color, mixVal );
    
}
