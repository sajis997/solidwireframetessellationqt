#version 430 compatibility

//vertex position in the model space
in vec3 Position;

void main()
{
   //pass-through the vertex position in
   //model coordinates
   gl_Position = vec4( Position,1.0 );	
}