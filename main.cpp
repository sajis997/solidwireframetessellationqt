#include <QGuiApplication>
#include <QtQuick/QQuickView>

#include "TessellationSceneItem.h"

int main(int argc, char **argv)
{
    QGuiApplication app(argc,argv);

    /*
     * any QObject derived C++ class can be registered as the definiton of a QML object type. Once a class is registered
     * with the QML type system, the class can be declared and instantiated like any other object type
     * from QML code. Once created, a class instance can be manipulated from QML. And the properties, methods and signals
     * of any QObject-derived class are accessible from QML code.
     * */
    qmlRegisterType<TessellationSceneItem>("TeapotTessellation",1,0,"TessellationSceneItem");

    QQuickView view;
    //The view will automatically resize the root item to the size of the view.
    view.setResizeMode(QQuickView::SizeRootObjectToView);
    view.setSource(QUrl("qrc:///qml/main.qml"));
    view.show();

    return app.exec();
}
