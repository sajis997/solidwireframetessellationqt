#ifndef TEAPOTTESSELLATION_H
#define TEAPOTTESSELLATION_H

#include <QOpenGLFunctions_4_3_Compatibility>


#include "defines.h"
#include "AbstractScene.h"

class GLSLShader;
class TeapotVboPatch;


/*
 * we need to take care of rendering. This instance needs to be separated from the QQuickItem
 * because the item lives in the GUI thread and the rendering potentially happens in the
 * rendering thread
 *
 * the item lives in the GUI thread
 * */
class TeapotTessellation : public AbstractScene, protected QOpenGLFunctions_4_3_Compatibility
{
    Q_OBJECT

public:

    explicit TeapotTessellation(QObject *parent = 0);
    ~TeapotTessellation();

    //scene related functions
    virtual void initialise();
    virtual void update(float t);
    virtual void render();
    virtual void resize(int w,int h);

    void setViewportSize(int width,int height);

    void setOuterTessellationFactor(float);
    void setInnerTessellationFactor(float);

    GLfloat innerTessellationFactor();
    GLfloat outerTessellationFactor();


signals:

    void tessellationLevelChanged(float);


public slots:

    void paint();

private:

    void loadShaders();

    GLSLShader *mTessellationShader;

    TeapotVboPatch *mTeapotPatch;


    GLfloat mInnerTessellationFactor;

    GLfloat mOuterTessellationFactor;

    glm::mat4 ModelViewMatrix;
    glm::mat4 ProjectionMatrix;
    glm::mat4 ViewportMatrix;

    glm::mat4 ModelMatrix;
    glm::mat4 ViewMatrix;

    glm::mat3 NormalMatrix;

    float mCurrentTime;

    float mZoom;

    bool mInitialized;

    int mViewportWidth;
    int mViewportHeight;
};

#endif // TEAPOTTESSELLATION_H
