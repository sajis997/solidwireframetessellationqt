#ifndef GLSLSHADER_H
#define GLSLSHADER_H

#include "defines.h"

#include <QOpenGLFunctions_4_3_Compatibility>

using namespace std;

class GLSLShader : protected QOpenGLFunctions_4_3_Compatibility
{
public:
   GLSLShader(void);
   ~GLSLShader(void);	
   void LoadFromString(GLenum whichShader, const string& source);
   void LoadFromFile(GLenum whichShader, const string& filename);
   void CreateAndLinkProgram();
   void Use();
   void UnUse();
   void AddAttribute(const string& attribute);
   void AddUniform(const string& uniform);
   
   GLuint getAttribute(const std::string& );
   GLuint getUniform(const std::string&);

   //An indexer that returns the location of the attribute/uniform
   GLuint operator[](const string& attribute);
   GLuint operator()(const string& uniform);
   void DeleteShaderProgram();
   
private:

   enum ShaderType
   {
      VERTEX_SHADER,
      FRAGMENT_SHADER,
      GEOMETRY_SHADER,
      TESS_CONTROL_SHADER,
      TESS_EVALUATION_SHADER,
      MAX_SHADERS
   };

   GLuint _program;
   int _totalShaders;

   GLuint _shaders[MAX_SHADERS];

   std::map<string,GLuint> _attributeList;
   std::map<string,GLuint> _uniformLocationList;

};	


#endif
